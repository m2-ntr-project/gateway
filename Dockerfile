FROM tomcat:9.0

COPY ./target/gateway-1.0.0.war /usr/local/tomcat/webapps/gateway.war
COPY ./server.xml /usr/local/tomcat/conf/server.xml

EXPOSE 8080

CMD ["catalina.sh", "run"]