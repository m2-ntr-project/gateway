package com.gateway.payment;

import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class PaymentEndpoint {

    private static final String NAMESPACE_URI = "http://localhost:8080/payment";

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "payRequest")
    @ResponsePayload
    public PayResponse getPayResponse(@RequestPayload PayRequest request) {
        PayResponse response = new PayResponse();
        response.setResponse(true);

        return response;
    }
}