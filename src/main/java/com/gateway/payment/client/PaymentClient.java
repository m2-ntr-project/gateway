package com.gateway.payment.client;

import com.gateway.payment.client.gen.PayRequest;
import com.gateway.payment.client.gen.PayResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class PaymentClient extends WebServiceGatewaySupport {

    private static final Logger logger = LoggerFactory.getLogger(PaymentClient.class);

    public PayResponse getPayResponse(String email, String password, Double amount) {

        PayRequest request = new PayRequest();
        request.setEmail(email);
        request.setPassword(password);
        request.setAmount(amount);

        logger.info("Requesting payment for " + email);

        PayResponse response = (PayResponse) getWebServiceTemplate().marshalSendAndReceive(request);

        return response;
    }

}